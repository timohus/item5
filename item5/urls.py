from django.conf.urls import url

from main import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about', views.about, name='about'),
    url(r'^contact', views.contact, name='contact'),
    url(r'^', views.no_such_page),  # Catch-all url. Fires when nothing from above was a match
]
